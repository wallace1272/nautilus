/* 
 * This file is part of the Nautilus AeroKernel developed
 * by the Hobbes and V3VEE Projects with funding from the 
 * United States National  Science Foundation and the Department of Energy.  
 *
 * The V3VEE Project is a joint project between Northwestern University
 * and the University of New Mexico.  The Hobbes Project is a collaboration
 * led by Sandia National Laboratories that includes several national 
 * laboratories and universities. You can find out more at:
 * http://www.v3vee.org  and
 * http://xstack.sandia.gov/hobbes
 *
 * Copyright (c) 2015, Kyle C. Hale <kh@u.northwestern.edu>
 * Copyright (c) 2015, The V3VEE Project  <http://www.v3vee.org> 
 *                     The Hobbes Project <http://xstack.sandia.gov/hobbes>
 * All rights reserved.
 *
 * Authors: Kyle C. Hale <kh@u.northwestern.edu>
 *          Yang Wu, Fei Luo and Yuanhui Yang
 *          {YangWu2015, FeiLuo2015, YuanhuiYang2015}@u.northwestern.edu
 *          Peter Dinda <pdinda@northwestern.edu>
 * This is free software.  You are permitted to use,
 * redistribute, and modify it as specified in the file "LICENSE.txt".
 */

#include <nautilus/nautilus.h>
#include <nautilus/irq.h>
#include <nautilus/thread.h>
#include <dev/vesa.h>
#include <dev/kbd.h>
#include <nautilus/vc.h>



#ifndef NAUT_CONFIG_DEBUG_KBD
#undef DEBUG_PRINT
#define DEBUG_PRINT(fmt, args...) 
#endif

#define ERROR(fmt, args...) ERROR_PRINT("kbd: " fmt, ##args)
#define DEBUG(fmt, args...) DEBUG_PRINT("kbd: " fmt, ##args)
#define INFO(fmt, args...)  INFO_PRINT("kbd: " fmt, ##args)


#define SCAN_MAX_QUEUE 16

static enum { VC_START, VC_CONTEXT, VC_PREV, VC_NEXT, VC_MENU, VC_PREV_ALT, VC_NEXT_ALT, VC_MENU_ALT}  switch_state = VC_START;
static uint8_t switcher_num_queued=0;
static nk_scancode_t switcher_scancode_queue[SCAN_MAX_QUEUE];

#define KB_KEY_RELEASE 0x80

static const nk_keycode_t NoShiftNoCaps[] = {
    KEY_UNKNOWN, ASCII_ESC, '1', '2',   /* 0x00 - 0x03 */
    '3', '4', '5', '6',                 /* 0x04 - 0x07 */
    '7', '8', '9', '0',                 /* 0x08 - 0x0B */
    '-', '=', ASCII_BS, '\t',           /* 0x0C - 0x0F */
    'q', 'w', 'e', 'r',                 /* 0x10 - 0x13 */
    't', 'y', 'u', 'i',                 /* 0x14 - 0x17 */
    'o', 'p', '[', ']',                 /* 0x18 - 0x1B */
    '\r', KEY_LCTRL, 'a', 's',          /* 0x1C - 0x1F */
    'd', 'f', 'g', 'h',                 /* 0x20 - 0x23 */
    'j', 'k', 'l', ';',                 /* 0x24 - 0x27 */
    '\'', '`', KEY_LSHIFT, '\\',        /* 0x28 - 0x2B */
    'z', 'x', 'c', 'v',                 /* 0x2C - 0x2F */
    'b', 'n', 'm', ',',                 /* 0x30 - 0x33 */
    '.', '/', KEY_RSHIFT, KEY_PRINTSCRN, /* 0x34 - 0x37 */
    KEY_LALT, ' ', KEY_CAPSLOCK, KEY_F1, /* 0x38 - 0x3B */
    KEY_F2, KEY_F3, KEY_F4, KEY_F5,     /* 0x3C - 0x3F */
    KEY_F6, KEY_F7, KEY_F8, KEY_F9,     /* 0x40 - 0x43 */
    KEY_F10, KEY_NUMLOCK, KEY_SCRLOCK, KEY_KPHOME,  /* 0x44 - 0x47 */
    KEY_KPUP, KEY_KPPGUP, KEY_KPMINUS, KEY_KPLEFT,  /* 0x48 - 0x4B */
    KEY_KPCENTER, KEY_KPRIGHT, KEY_KPPLUS, KEY_KPEND,  /* 0x4C - 0x4F */
    KEY_KPDOWN, KEY_KPPGDN, KEY_KPINSERT, KEY_KPDEL,  /* 0x50 - 0x53 */
    KEY_SYSREQ, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN,  /* 0x54 - 0x57 */
};

static const nk_keycode_t ShiftNoCaps[] = {
    KEY_UNKNOWN, ASCII_ESC, '!', '@',   /* 0x00 - 0x03 */
    '#', '$', '%', '^',                 /* 0x04 - 0x07 */
    '&', '*', '(', ')',                 /* 0x08 - 0x0B */
    '_', '+', ASCII_BS, '\t',           /* 0x0C - 0x0F */
    'Q', 'W', 'E', 'R',                 /* 0x10 - 0x13 */
    'T', 'Y', 'U', 'I',                 /* 0x14 - 0x17 */
    'O', 'P', '{', '}',                 /* 0x18 - 0x1B */
    '\r', KEY_LCTRL, 'A', 'S',          /* 0x1C - 0x1F */
    'D', 'F', 'G', 'H',                 /* 0x20 - 0x23 */
    'J', 'K', 'L', ':',                 /* 0x24 - 0x27 */
    '"', '~', KEY_LSHIFT, '|',          /* 0x28 - 0x2B */
    'Z', 'X', 'C', 'V',                 /* 0x2C - 0x2F */
    'B', 'N', 'M', '<',                 /* 0x30 - 0x33 */
    '>', '?', KEY_RSHIFT, KEY_PRINTSCRN, /* 0x34 - 0x37 */
    KEY_LALT, ' ', KEY_CAPSLOCK, KEY_F1, /* 0x38 - 0x3B */
    KEY_F2, KEY_F3, KEY_F4, KEY_F5,     /* 0x3C - 0x3F */
    KEY_F6, KEY_F7, KEY_F8, KEY_F9,     /* 0x40 - 0x43 */
    KEY_F10, KEY_NUMLOCK, KEY_SCRLOCK, KEY_KPHOME,  /* 0x44 - 0x47 */
    KEY_KPUP, KEY_KPPGUP, KEY_KPMINUS, KEY_KPLEFT,  /* 0x48 - 0x4B */
    KEY_KPCENTER, KEY_KPRIGHT, KEY_KPPLUS, KEY_KPEND,  /* 0x4C - 0x4F */
    KEY_KPDOWN, KEY_KPPGDN, KEY_KPINSERT, KEY_KPDEL,  /* 0x50 - 0x53 */
    KEY_SYSREQ, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN,  /* 0x54 - 0x57 */
};

static const nk_keycode_t NoShiftCaps[] = {
    KEY_UNKNOWN, ASCII_ESC, '1', '2',   /* 0x00 - 0x03 */
    '3', '4', '5', '6',                 /* 0x04 - 0x07 */
    '7', '8', '9', '0',                 /* 0x08 - 0x0B */
    '-', '=', ASCII_BS, '\t',           /* 0x0C - 0x0F */
    'Q', 'W', 'E', 'R',                 /* 0x10 - 0x13 */
    'T', 'Y', 'U', 'I',                 /* 0x14 - 0x17 */
    'O', 'P', '[', ']',                 /* 0x18 - 0x1B */
    '\r', KEY_LCTRL, 'A', 'S',          /* 0x1C - 0x1F */
    'D', 'F', 'G', 'H',                 /* 0x20 - 0x23 */
    'J', 'K', 'L', ';',                 /* 0x24 - 0x27 */
    '\'', '`', KEY_LSHIFT, '\\',        /* 0x28 - 0x2B */
    'Z', 'X', 'C', 'V',                 /* 0x2C - 0x2F */
    'B', 'N', 'M', ',',                 /* 0x30 - 0x33 */
    '.', '/', KEY_RSHIFT, KEY_PRINTSCRN, /* 0x34 - 0x37 */
    KEY_LALT, ' ', KEY_CAPSLOCK, KEY_F1, /* 0x38 - 0x3B */
    KEY_F2, KEY_F3, KEY_F4, KEY_F5,     /* 0x3C - 0x3F */
    KEY_F6, KEY_F7, KEY_F8, KEY_F9,     /* 0x40 - 0x43 */
    KEY_F10, KEY_NUMLOCK, KEY_SCRLOCK, KEY_KPHOME,  /* 0x44 - 0x47 */
    KEY_KPUP, KEY_KPPGUP, KEY_KPMINUS, KEY_KPLEFT,  /* 0x48 - 0x4B */
    KEY_KPCENTER, KEY_KPRIGHT, KEY_KPPLUS, KEY_KPEND,  /* 0x4C - 0x4F */
    KEY_KPDOWN, KEY_KPPGDN, KEY_KPINSERT, KEY_KPDEL,  /* 0x50 - 0x53 */
    KEY_SYSREQ, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN,  /* 0x54 - 0x57 */
};

static const nk_keycode_t ShiftCaps[] = {
    KEY_UNKNOWN, ASCII_ESC, '!', '@',   /* 0x00 - 0x03 */
    '#', '$', '%', '^',                 /* 0x04 - 0x07 */
    '&', '*', '(', ')',                 /* 0x08 - 0x0B */
    '_', '+', ASCII_BS, '\t',           /* 0x0C - 0x0F */
    'q', 'w', 'e', 'r',                 /* 0x10 - 0x13 */
    't', 'y', 'u', 'i',                 /* 0x14 - 0x17 */
    'o', 'p', '{', '}',                 /* 0x18 - 0x1B */
    '\r', KEY_LCTRL, 'a', 's',          /* 0x1C - 0x1F */
    'd', 'f', 'g', 'h',                 /* 0x20 - 0x23 */
    'j', 'k', 'l', ':',                 /* 0x24 - 0x27 */
    '"', '~', KEY_LSHIFT, '|',          /* 0x28 - 0x2B */
    'z', 'x', 'c', 'v',                 /* 0x2C - 0x2F */
    'b', 'n', 'm', '<',                 /* 0x30 - 0x33 */
    '>', '?', KEY_RSHIFT, KEY_PRINTSCRN, /* 0x34 - 0x37 */
    KEY_LALT, ' ', KEY_CAPSLOCK, KEY_F1, /* 0x38 - 0x3B */
    KEY_F2, KEY_F3, KEY_F4, KEY_F5,     /* 0x3C - 0x3F */
    KEY_F6, KEY_F7, KEY_F8, KEY_F9,     /* 0x40 - 0x43 */
    KEY_F10, KEY_NUMLOCK, KEY_SCRLOCK, KEY_KPHOME,  /* 0x44 - 0x47 */
    KEY_KPUP, KEY_KPPGUP, KEY_KPMINUS, KEY_KPLEFT,  /* 0x48 - 0x4B */
    KEY_KPCENTER, KEY_KPRIGHT, KEY_KPPLUS, KEY_KPEND,  /* 0x4C - 0x4F */
    KEY_KPDOWN, KEY_KPPGDN, KEY_KPINSERT, KEY_KPDEL,  /* 0x50 - 0x53 */
    KEY_SYSREQ, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN,  /* 0x54 - 0x57 */
};



#define KBD_DATA_REG 0x60
#define KBD_ACK_REG  0x61
#define KBD_CMD_REG  0x64
#define KBD_STATUS_REG  0x64

#define KBD_RELEASE  0x80

#define STATUS_OUTPUT_FULL 0x1

static int inited = 0;
static nk_keycode_t flags = 0;
#define SCREEN_X 1024
#define SCREEN_Y 768
uint32_t cur_x = SCREEN_X/2,cur_y = SCREEN_Y/2;

static void setup_screen()
{
    vesa_mode_t mode, orig_mode;
    struct vesa_mode_request r;

    r.width=SCREEN_X;
    r.height=SCREEN_Y;
    r.bpp=32;
    r.text=0;
    r.lfb=1;

    INFO("Looking for a 1024x768x32 graphics mode\n");

    if (vesa_find_matching_mode(&r,&mode)) { 
	INFO("Cannot find a matching mode\n");
    } else {
	INFO("Found mode - now switching to it\n");
	if (vesa_set_cur_mode(mode)) {
	    ERROR("Failed to set current mode\n");
	    return;
	}

    }
}
#define SCALE 1024
static void update_screen(int x, int y){
	cur_x = x/SCALE + SCREEN_X/2;
	cur_y = y/SCALE + SCREEN_Y/2;
	cur_x %= SCREEN_X;
	cur_y %= SCREEN_Y;
	vesa_draw_pixel(cur_x, cur_y, 255,255,255);
	
}

static void queue_scancode(nk_scancode_t scan)
{
  if (switcher_num_queued==SCAN_MAX_QUEUE) { 
    ERROR("Out of room in switcher queue\n");
  } else {
    switcher_scancode_queue[switcher_num_queued++] = scan;
  }
}

static void dequeue_scancodes()
{
  int i;

  for (i=0;i<switcher_num_queued;i++) { 
    nk_vc_handle_input(switcher_scancode_queue[i]);
  }

  switcher_num_queued=0;
}

static void flush_scancodes()
{
  switcher_num_queued=0;
}

nk_keycode_t kbd_translate(nk_scancode_t scan)
{
  int release;
  const nk_keycode_t *table=0;
  nk_keycode_t cur;
  nk_keycode_t flag;
  

  // update the flags

  release = scan & KB_KEY_RELEASE;
  scan &= ~KB_KEY_RELEASE;

  if (flags & KEY_CAPS_FLAG) { 
    if (flags & KEY_SHIFT_FLAG) { 
      table = ShiftCaps;
    } else {
      table = NoShiftCaps;
    }
  } else {
    if (flags & KEY_SHIFT_FLAG) { 
      table = ShiftNoCaps;
    } else {
      table = NoShiftNoCaps;
    }
  }    
  
  cur = table[scan];
  
  flag = 0;
  switch (cur) { 
  case KEY_LSHIFT:
  case KEY_RSHIFT:
    flag = KEY_SHIFT_FLAG;
    break;
  case KEY_LCTRL:
  case KEY_RCTRL:
    flag = KEY_CTRL_FLAG;
    break;
  case KEY_LALT:
  case KEY_RALT:
    flag = KEY_ALT_FLAG;
    break;
  case KEY_CAPS_FLAG:
    flag = KEY_CAPS_FLAG;
    break;
  default:
    goto do_noflags;
    break;
  }
  
  // do_flags:
  if (flag==KEY_CAPS_FLAG) { 
    if ((!release) && (flags & KEY_CAPS_FLAG)) { 
      // turn off caps lock on second press
      flags &= ~KEY_CAPS_FLAG;
      flag = 0;
    } 
  }

  if (release) {
    flags &= ~(flag);
  } else {
    flags |= flag;
  }

  DEBUG("Handled flag change (flags now 0x%x)\n", flags);

  return NO_KEY;

 do_noflags:
  if (release) { 
    DEBUG("Handled key release (returning 0x%x)\n", flags|cur);
    return flags | cur;
  } else {
    return NO_KEY;
  }
}


#define ALT 0x38
#define ONE 0x2
#define TWO 0x3
#define TILDE 0x29
 
/*
  Special case handling of scancodes relating to virtual
  console switching.  This happens regardless of whether
  the current console is raw or cooked.   If the console
  switch state machine doesn't recognize the scancode (or
  the ones it has accumulated), it passes them to the 
  virtual console subsystem for further processing.  
*/
static int switcher(nk_scancode_t scan) 
{
  queue_scancode(scan);

  switch (switch_state) { 
  case VC_START:
    if (scan==ALT) { 
      DEBUG("VC CONTEXT\n");
      switch_state = VC_CONTEXT;
    } else {
      DEBUG("VC RESTART\n");
      dequeue_scancodes();
      switch_state = VC_START;
    }
    break;
  case VC_CONTEXT:
    if (scan==ONE) { 
      DEBUG("VC PREV\n");
      switch_state = VC_PREV;
    } else if (scan==TWO) { 
      DEBUG("VC NEXT\n");
      switch_state = VC_NEXT;
    } else if (scan==TILDE) {
      DEBUG("VC MENU\n");
      switch_state = VC_MENU;
    } else {
      DEBUG("VC RESTART\n");
      dequeue_scancodes();
      switch_state = VC_START;
    }
    break;
  case VC_PREV:
    if (scan==(ONE | KB_KEY_RELEASE)) { 
      DEBUG("VC PREV ALT\n");
      switch_state = VC_PREV_ALT;
    } else {
      DEBUG("VC RESTART\n");
      switch_state = VC_START;
      dequeue_scancodes();
    }
    break;
  case VC_NEXT:
    if (scan==(TWO | KB_KEY_RELEASE)) { 
      DEBUG("VC NEXT ALT\n");
      switch_state = VC_NEXT_ALT;
    } else {
      DEBUG("VC RESTART\n");
      switch_state = VC_START;
      dequeue_scancodes();
    }
    break;
  case VC_MENU:
    if (scan==(TILDE | KB_KEY_RELEASE)) { 
      DEBUG("VC MENU ALT\n");
      switch_state = VC_MENU_ALT;
    } else {
      DEBUG("VC RESTART\n");
      switch_state = VC_START;
      dequeue_scancodes();
    }
    break;
  case VC_PREV_ALT:
    if (scan==(ALT | KB_KEY_RELEASE)) { 
      DEBUG("VC SWITCH TO PREV\n");
      nk_switch_to_prev_vc();
      switch_state = VC_START;
      flush_scancodes();
    } else {
      DEBUG("VC RESTART\n");
      dequeue_scancodes();
      switch_state = VC_START;
    }
    break;
  case VC_NEXT_ALT:
    if (scan==(ALT | KB_KEY_RELEASE)) { 
      DEBUG("VC SWITCH TO NEXT\n");
      nk_switch_to_next_vc();
      switch_state = VC_START;
      flush_scancodes();
    } else {
      DEBUG("VC RESTART\n");
      dequeue_scancodes();
      switch_state = VC_START;
    }
    break;  
  case VC_MENU_ALT:
    if (scan==(ALT | KB_KEY_RELEASE)) { 
      DEBUG("VC SWITCH TO MENU\n");
      nk_switch_to_vc_list();
      switch_state = VC_START;
      flush_scancodes();
    } else {
      DEBUG("VC RESTART\n");
      dequeue_scancodes();
      switch_state = VC_START;
    }
    break;
  default:
    DEBUG("VC HUH?\n");
    dequeue_scancodes();
    switch_state = VC_START;
    break;
  }
  return 0;
}


static int 
kbd_handler (excp_entry_t * excp, excp_vec_t vec)
{
  
  uint8_t status;
  nk_scancode_t scan;
  uint8_t key;
  uint8_t flag;
  
  status = inb(KBD_CMD_REG);

  io_delay();

  if ((status & STATUS_OUTPUT_FULL) != 0) {
    scan  = inb(KBD_DATA_REG);
    DEBUG("Keyboard: status=0x%x, scancode=0x%x\n", status, scan);
    io_delay();
    
    
#if NAUT_CONFIG_THREAD_EXIT_KEYCODE == 0xc4
    // Vestigal debug handling to force thread exit
    if (scan == 0xc4) {
      void * ret = NULL;
      IRQ_HANDLER_END();
      kbd_reset();
      nk_thread_exit(ret);
    }
#endif
    
    switcher(scan);
    
  }

  IRQ_HANDLER_END();
  return 0;
}

typedef union ps2_status {
    uint8_t  val;
    struct {
	uint8_t obf:1;  // output buffer full (0=> can read from 0x60)
	uint8_t ibf:1;  // input buffer full (0=> can write to either port)
	uint8_t sys:1;  // set on successful reset
	uint8_t a2:1;   // last address (0=>0x60, 1=>0x64)
	uint8_t inh:1;  // keyboard inhibit, active low
	uint8_t mobf:1; // mouse buffer full
	uint8_t to:1;   // timeout
	uint8_t perr:1; // parity error
    } ;
} __packed ps2_status_t;

typedef union ps2_cmd {
    uint8_t  val;
    struct {
	uint8_t kint:1; // enable keyboard interrupts
	uint8_t mint:1; // enable mouse interrupts
	uint8_t sys:1;  // set/clear sysflag / do BAT
	uint8_t rsv1:1; 
	uint8_t ken:1;  // keyboard enable, active low
	uint8_t men:1;  // mouse enable, active low
	uint8_t xlat:1; // do scancode translation
	uint8_t rsv2:1;
    } ;
} __packed ps2_cmd_t;


// INPUT => want to write to 0x60 or 0x64
// OUTPUT => want to read from 0x60
typedef enum {INPUT, OUTPUT} wait_t;

static uint16_t mouse_packet[4];
static int cur_mouse_packet_byte=0;


union mouse_packet {
        uint8_t data[3];
            struct {
                    uint8_t  bl:1;
                    uint8_t  br:1;
                    uint8_t  bm:1;
                    uint8_t  a0:1;
                    uint8_t  xs:1;
                    uint8_t  ys:1;
                    uint8_t  xo:1;
                    uint8_t  yo:1;
                    uint8_t  xm;
                    uint8_t  ym;
        } __packed;
} __packed; 

struct cooked_mouse {
    int dx;
    int dy;
    uint8_t leftclick;
    uint8_t rightclick;
    uint8_t middleclick;
};


static union mouse_packet cur_mouse_packet;

int  mouse_num_queued=0;
struct cooked_mouse cooked_queue[1024];
void queue_mouse_info(int dx, int dy, uint8_t lc, uint8_t rc, uint8_t mc){
    struct cooked_mouse data;
    data.dx = dx;
    data.dy = dy;
    data.leftclick = lc;
    data.rightclick = rc;
    data.middleclick = mc;

    //really basic stuff here;
    cooked_queue[mouse_num_queued % 1024] = data;
    mouse_num_queued++;

}

static int ps2_wait(wait_t w)
{
    volatile ps2_status_t status;

    do {
	status.val = inb(KBD_STATUS_REG);
	//	INFO("obf=%d ibf=%d req=%s\n",status.obf, status.ibf, w==INPUT ? "input" : "output");
    } while (!( (status.obf && (w==OUTPUT) ) ||
	       ((!status.ibf) && (w==INPUT)))    );

    return 0;
} 

static void mouse_write(uint8_t d)
{
    ps2_wait(INPUT);
    outb(0xd4, KBD_CMD_REG);  // address mouse
    ps2_wait(INPUT);
    outb(d, KBD_DATA_REG);
}

static uint8_t mouse_read()
{
    ps2_wait(OUTPUT);
    return inb(KBD_DATA_REG);
}

static uint8_t mouse_read_after_ack()
{
    uint8_t rc;
    ps2_wait(OUTPUT);
    
    while ((rc = inb(KBD_DATA_REG)) == 0xfa){
        continue;
    }
    return rc;
}


static int mouse_reset()
{
    ps2_cmd_t cmd;
    uint32_t rc;

    INFO("Reseting mouse\n");
    
    

    // enable mouse in PS2 controller
    ps2_wait(INPUT);
    outb(0xa8,KBD_CMD_REG); 

    INFO("Mouse enabled on PS2 controller\n");

    // enable mouse interrupts on PS2 controller
    ps2_wait(INPUT);
    outb(0x20,KBD_CMD_REG); // prepare to read command word
    ps2_wait(OUTPUT);
    cmd.val = inb(KBD_DATA_REG);
    INFO("Initial command word is 0x%x\n",cmd.val);
    cmd.mint = 1;
    cmd.men = 0;  // enable is active low
    ps2_wait(INPUT);
    outb(0x60, KBD_CMD_REG); // prepare to write command word
    ps2_wait(INPUT);
    outb(cmd.val, KBD_DATA_REG);
    INFO("PS2 command word is now 0x%x\n",cmd.val);

    // reset mouse
    INFO("Reset\n");
    mouse_write(0xff);  
    rc = mouse_read(); 
    rc = (rc << 8) | mouse_read();
    rc = (rc << 8) | mouse_read();
    if (rc==0xfaaa00) {  // ACK SELF-TEST-PASSED DONE
	INFO("Mouse reset good\n");
    } else {
	INFO("Mouse reset failed (%x)\n",rc);
    }

    // determine mouse type
    mouse_write(0xf2);  
    rc = mouse_read(); 
    if (rc!=0xfa) { 
	INFO("On reset, mouse did not return ack, but rather 0x%x\n",rc);
    } else {
	rc = mouse_read();
	INFO("Mouse is of type 0x%x (%s)\n",rc, rc==0 ? "generic ps/2" : "something weird");
    }

    // configure mouse for defaults
    mouse_write(0xf6);  
    rc = mouse_read(); 
    if (rc!=0xfa) { 
	INFO("On config, mouse did not return ack, but rather 0x%x\n",rc);
    }

    // set stream mode (shouldn't have to do this)
    //mouse_write(0xea);
    //rc = mouse_read();
    //if (rc!=0xfa) { 
	//INFO("Cannot set stream mode (%x)\n",rc);
    //}

    // enable mouse data reporting
    mouse_write(0xf4);
    rc = mouse_read(); 
    if (rc!=0xfa) { 
	INFO("On enable, mouse did not return ack, but rather 0x%x\n",rc);
    }
   
    

    mouse_write(0xe8);
    mouse_read();
    mouse_write(0x03);
    mouse_read();

    mouse_write(0xe9);
    rc = mouse_read();
    if (rc != 0xfa){
      INFO("WhAT?");
    }
    
    uint8_t status[3]; 
    status[0] = mouse_read();
    status[1] = mouse_read();
    status[2] = mouse_read();

    INFO("status is %x %x %x \n" , status[0], status[1], status[2]);
    /*mouse_write(0xf2); // want to see mouse ID
    rc = mouse_read_after_ack();
   
    INFO("THE MOUSE ID IS: %x\n", rc);
    //enable 4th packet and scroll wheel
    mouse_write(0xf3);
    mouse_write(0xc8); // the magic sequence set sample rate to 200->100->80 
    mouse_write(0xf3);
    mouse_write(0x64);
    mouse_write(0x50);

    mouse_write(0xf2);
    rc = mouse_read_after_ack();
    if (rc != 0x3){
        INFO("Failed to enable scroll wheel\n");
    }

    INFO("THE MOUSE ID IS: %x\n", rc);
    */
    INFO("Mouse reset done\n");

    return 0;
}


static int  x_posn_queue[SCAN_MAX_QUEUE];
static int  y_posn_queue[SCAN_MAX_QUEUE];
static void queue_mouse_posn(int x_posn, int y_posn)
{
  if (mouse_num_queued == SCAN_MAX_QUEUE) { 
  //  ERROR("Out of room in switcher queue\n");
      x_posn_queue[0] = x_posn_queue[SCAN_MAX_QUEUE-1];
      y_posn_queue[0] = y_posn_queue[SCAN_MAX_QUEUE-1];
      mouse_num_queued = 1;
  } 
    x_posn_queue[mouse_num_queued] = x_posn;
    y_posn_queue[mouse_num_queued] = y_posn;
    mouse_num_queued++; 
  
}
typedef long int64_t;

int64_t x,y;



static int mouse_handler (excp_entry_t * excp, excp_vec_t vec)
{
    ps2_status_t status;
    int count=0;
    int first = 1; 
	if (!inited){
		setup_screen();
		inited = 1;
	}
    //    INFO("MOUSE IRQ\n");

  while (1) {
    status.val = inb(KBD_CMD_REG);

    if (count==0) { 
        INFO("Initial status is 0x%x\n", status.val);
    }

    if (!status.mobf) {
        if (count==0) { 
      INFO("Strange: zero bytes in and status is 0x%x and first = %d\n",status.val, first);
        }
        break;
    }
    
    count++;
    first = 0; 
    //INFO("Status = 0x%x\n",status.val);
    
    uint8_t data;
    data = inb(KBD_DATA_REG);
    //INFO("Have mouse data (%x)\n",(uint32_t)data);
    cur_mouse_packet.data[cur_mouse_packet_byte] = data;
    cur_mouse_packet_byte++;
    if (cur_mouse_packet_byte==3) {
        // done with packet
      INFO("Mouse Packet: %x %x %x\n", cur_mouse_packet.data[0], cur_mouse_packet.data[1], cur_mouse_packet.data[2]);
#if 0
        INFO("Mouse Packet: buttons: %s %s %s\n",
       cur_mouse_packet.bl ? "down" : "up",
       cur_mouse_packet.bm ? "down" : "up",
       cur_mouse_packet.br ? "down" : "up");
        INFO("Mouse Packet: overflows: xo: %d yo: %d\n",
       cur_mouse_packet.xo, cur_mouse_packet.yo);
        INFO("Mouse Packet: dx: %d dy: %d\n",
       (int) cur_mouse_packet.xm - (cur_mouse_packet.xs * 0x100),
       (int) cur_mouse_packet.ym - (cur_mouse_packet.ys * 0x100));
#endif
        int dx = cur_mouse_packet.xm - ((cur_mouse_packet.data[0] << 4) & 0x100);
        int dy = cur_mouse_packet.ym - ((cur_mouse_packet.data[0] << 3) & 0x100);
        uint8_t leftclick = cur_mouse_packet.bl;
        uint8_t rightclick = cur_mouse_packet.br;
        uint8_t middleclick = cur_mouse_packet.bm;
        
        queue_mouse_info(dx, dy, leftclick,rightclick,middleclick);
        

        //cur_cooked_mouse.leftclick = cur_mouse_packet

        if (cur_mouse_packet.xo || cur_mouse_packet.yo) { 
      INFO("Mouse Packet Ignored: overflows: xo: %d yo: %d\n",
           cur_mouse_packet.xo, cur_mouse_packet.yo);
        } else {
      x += dx;
      y += dy;

        update_screen(x,y);    	
      cur_mouse_packet_byte=0;
      //INFO("                                                         \rMouse: %ld\t%ld\r", x, y);
      INFO("Mouse: %ld\t%ld\n", x, y);
        }
        cur_mouse_packet_byte=0;
      }
    
    }

    INFO("Handled %d mouse bytes during this interrupt\n",count);
    if (count % 3){
        ERROR("NOT MULTIPLE OF 3 HANDLED");
        panic("panic mouse");
    }
    IRQ_HANDLER_END();
    return 0;

}


/*
static int mouse_handler (excp_entry_t * excp, excp_vec_t vec)
{
    ps2_status_t status;
    
    uint32_t rc;
    int count = 0; 
   //INFO("MOUSE IRQ\n");
	if (!inited){
		setup_screen();
		inited = 1;
	}
    
    while (1){
        status.val = inb(KBD_CMD_REG);

        if (count == 0){
            INFO("Initial status is 0x%x\n", status.val);
        }

        if (!status.mobf) { 
            if (count == 0){
            //INFO("Have mouse data\n");
                INFO("Strange: zero bytes in and status is 0x%x\n",status.val);
            }
            break;
        }
        
        count++;

       // INFO("Status = 0x%x\n",status.val);
       
        mouse_packet[cur_mouse_packet_byte] = inb(KBD_DATA_REG);
        cur_mouse_packet_byte++;
        if (cur_mouse_packet_byte==3) {
            // done with packet
            INFO("Mouse Packet: %x %x %x\n", mouse_packet[0], mouse_packet[1], mouse_packet[2]);
            cur_mouse_packet_byte=0;
            if ((mouse_packet[0] & 0x80) || (mouse_packet[0] & 0x40)){
                INFO("X OR Y OVERFLOW HAS BEEN SET");
                goto out;     
             }
        
            int rel_x = mouse_packet[1] - ((mouse_packet[0] << 4) & 0x100);
            int rel_y = mouse_packet[2] - ((mouse_packet[0] << 3) & 0x100);

            if (mouse_num_queued >0){
            queue_mouse_posn(rel_x + x_posn_queue[mouse_num_queued-1], rel_y + y_posn_queue[mouse_num_queued-1]);
            }
            else{
                queue_mouse_posn(rel_x,rel_y);
            }
            INFO("X POSN: %i Y POSN: %i\n" , x_posn_queue[mouse_num_queued] , y_posn_queue[mouse_num_queued]);
        update_screen(x_posn_queue[mouse_num_queued] , y_posn_queue[mouse_num_queued]);    	
        cur_mouse_packet_byte = 0;
        mouse_packet[0] = 0;
        mouse_packet[1] = 0;
        mouse_packet[2] = 0;

	    }
    else {
	INFO("Unhandled mouse interrupt....\n");
	goto out;
    }
    }
 out:

    IRQ_HANDLER_END();
    return 0;

}*/

kbd_init (struct naut_info * naut)
{
  INFO("init\n");
  register_irq_handler(1, kbd_handler, NULL);
  register_irq_handler(12, mouse_handler, NULL);
  kbd_reset();
  mouse_reset();
  nk_unmask_irq(1);
  nk_unmask_irq(12);
  
  return 0;
}


int kbd_reset()
{
  INFO("reset\n");
  flags=0;
  return 0;
}



int kbd_deinit()
{
  INFO("deinit\n");
  return 0;
}




